package fr.ign.geobooking;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import com.webgeoservices.woosmapgeofencing.Woosmap;
import com.webgeoservices.woosmapgeofencing.WoosmapSettings;
import com.webgeoservices.woosmapgeofencingcore.database.*;


public class MainGeofencingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_geofencing);
    }
}